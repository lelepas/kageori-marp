---

marp: true
theme: Kageori
footer: <img src="../kageori.png">

---

# Lorem Ipsum
[@kageori_ar](https://twitter.com/kageori_ar)

---


### Lorem ipsum 

- dolor sit amet, consectetur adipiscing elit. Vestibulum aliquam urna vitae elit suscipit tincidunt sed at metus. **Fusce id nunc nunc.**

### Integer finibus turpis pretium odio dapibus

- eget auctor erat pharetra. Mauris pellentesque odio condimentum iaculis porttitor. 

---

# Proin sollicitudin

 Justo eget libero vehicula, ut accumsan dui rhoncus. Maecenas eleifend dapibus mi, id scelerisque neque. Suspendisse tempus risus a purus faucibus tincidunt.

![bg left](./img/top-t.png)


---

## Donec ornare mi sed orci accumsan

ac malesuada lectus viverra. Nam urna risus, lacinia eget dui a, vulputate dictum neque. In id ante eget urna interdum aliquet eu luctus diam. 


---

Maecenas id lacus cursus, feugiat lacus sit amet, iaculis metus. In diam augue, interdum et mauris vel, laoreet ullamcorper diam.

<hr>

 Morbi ipsum nunc, pulvinar at posuere dapibus, malesuada nec arcu. 
Integer sollicitudin elit odio, sit amet condimentum nibh sodales eu. Cras nibh leo, commodo sit amet eleifend a, ornare sed arcu.

---

# Phasellus interdum vestibulum mauris pretium molestie. 

Phasellus vel consequat justo. Vestibulum gravida enim consequat ante facilisis bibendum. 