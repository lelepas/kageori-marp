# kageori-marp

***

## Description
This is Kageori's marp theme.

## Install
https://www.kageori.com/2022/11/marp.html#how-to-use

## Example
![](example/img/1.png)
![](example/img/2.png)
![](example/img/3.png)
![](example/img/4.png)
![](example/img/5.png)
![](example/img/6.png)


## License

```
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <kageori> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */
 ```

